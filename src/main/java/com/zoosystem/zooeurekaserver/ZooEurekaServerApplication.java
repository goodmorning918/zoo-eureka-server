package com.zoosystem.zooeurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ZooEurekaServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(ZooEurekaServerApplication.class, args);
  }
}
